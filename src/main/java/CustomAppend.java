import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;

/**
 * Created by sa on 14.02.17.
 */
public class CustomAppend extends AppenderSkeleton {
    @Override
    protected void append(LoggingEvent loggingEvent) {
        System.out.println(loggingEvent.getLevel().toString() + "append");
    }

    @Override
    public void close() {

    }

    @Override
    public boolean requiresLayout() {
        return false;
    }
}
